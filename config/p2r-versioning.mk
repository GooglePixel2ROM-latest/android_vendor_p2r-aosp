#
# Copyright (C) 2018 The Google Pixel2ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Set release type (STABLE, BETA, ALPHA, UNOFFICIAL)
# Default is UNOFFICIAL
P2R_TYPE ?= UNOFFICIAL

# Set release version (x.y)
P2R_VERSION ?= 2.7

# Set default status
P2R_OFFICIAL ?= false
P2R_CERTS ?=
P2R_RELEASEKEYS ?=
P2R_STD_TRGT ?= otapackage

# Set release security patch version (first 3 letters of month, e.g. may for May)
P2R_SEC_PATCH_MONTH := jul

ifeq ($(P2R_OFFICIAL),true)
# triggered official build
P2R_TYPE := OFFICIAL
# define certificates
P2R_CERTS := vendor/p2r-aosp/security/certs
P2R_RELEASEKEYS := vendor/p2r-aosp/security/certs/releasekey
# change standard target
P2R_STD_TRGT := p2r-otapackage
# some flags
P2R_STFP_FLAGS := -e DynamiteLoader.apk= -e DynamiteModulesA.apk= -e DynamiteModulesB.apk= -e DynamiteModulesC.apk= -e DynamiteModulesD.apk= -e GoogleCertificates.apk=
endif
