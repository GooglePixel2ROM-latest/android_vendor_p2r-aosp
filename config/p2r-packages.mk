#
# Copyright (C) 2018 The Google Pixel2ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Include explicitly to work around GMS issues
PRODUCT_PACKAGES += \
	libprotobuf-cpp-full \
	librsjni

# Absolutely useless but someone still use it :C
PRODUCT_PACKAGES += \
	Stk

# If target builds vendor image from source put Pixel overlay, otherwise use Pixel framework standard overlay
ifeq ($(PRODUCT_USES_SOURCE_VENDOR_IMAGE), true)
# Build Pixel overlay
PRODUCT_PACKAGES += \
	PixelThemeOverlay

# Enable Pixel overlay on boot
PRODUCT_PROPERTY_OVERRIDES += ro.boot.vendor.overlay.theme=com.google.android.theme.pixel
else
# Add Pixel colors using legacy way directly to framework-res.apk
PRODUCT_PACKAGE_OVERLAYS += vendor/p2r-aosp/rro/legacy/overlay/pixel
endif

################################
#			       #
# Google Pixel2ROM Google Apps #
#			       #
################################
# INHERIT FROM P2R GAPPS CONFIG#
$(call inherit-product, vendor/p2r-gapps/p2r-gapps.mk)
