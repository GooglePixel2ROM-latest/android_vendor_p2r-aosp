#
# Copyright (C) 2018 The Google Pixel2ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# GoogleSans
PRODUCT_COPY_FILES += \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-BoldItalic.ttf:system/fonts/GoogleSans-BoldItalic.ttf \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-Bold.ttf:system/fonts/GoogleSans-Bold.ttf \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-Italic.ttf:system/fonts/GoogleSans-Italic.ttf \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-MediumItalic.ttf:system/fonts/GoogleSans-MediumItalic.ttf \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-Medium.ttf:system/fonts/GoogleSans-Medium.ttf \
	vendor/p2r-aosp/prebuilt/fonts/GoogleSans-Regular.ttf:system/fonts/GoogleSans-Regular.ttf
