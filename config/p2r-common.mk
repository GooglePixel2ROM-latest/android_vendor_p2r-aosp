#
# Copyright (C) 2018 The Google Pixel2ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Do not build utc date
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0

# Import properties from Pixel 2
PRODUCT_PROPERTY_OVERRIDES += \
	ro.com.android.dataroaming=false \
	ro.url.legal=http://www.google.com/intl/%s/mobile/android/basic/phone-legal.html \
	ro.url.legal.android_privacy=http://www.google.com/intl/%s/mobile/android/basic/privacy.html \
	ro.com.google.clientidbase=android-google \
	ro.carrier=unknown \
	ro.error.receiver.system.apps=com.google.android.gms \
	ro.setupwizard.enterprise_mode=1 \
	ro.atrace.core.services=com.google.android.gms,com.google.android.gms.ui,com.google.android.gms.persistent \
	ro.com.android.prov_mobiledata=false \
	ro.opa.eligible_device=true \
	ro.storage_manager.enabled=true \
	ro.com.google.ime.theme_id=5 \
	drm.service.enabled=true \
	media.mediadrmservice.enable=true \
	ro.setupwizard.rotation_locked=true \
	setupwizard.theme=glif_v2_light \
	ro.facelock.black_timeout=700 \
	ro.facelock.det_timeout=2500 \
	ro.facelock.rec_timeout=3500 \
	ro.facelock.est_max_time=600

# Enable ADB authentication <SECURITY>
PRODUCT_PROPERTY_OVERRIDES += ro.adb.secure=1

# Charger
PRODUCT_PACKAGES += \
	p2r-charger_res_images

# Don't compile SystemUITests (Anyway we do not use SystemUI)
EXCLUDE_SYSTEMUI_TESTS := true

# Overlay
PRODUCT_PACKAGE_OVERLAYS += vendor/p2r-aosp/overlay

# Other packages
$(call inherit-product, vendor/p2r-aosp/config/p2r-packages.mk)

# Boot animation
$(call inherit-product, vendor/p2r-aosp/config/p2r-bootanimation.mk)

# Fonts
$(call inherit-product, vendor/p2r-aosp/config/p2r-google_fonts.mk)

# Sounds
$(call inherit-product, vendor/p2r-aosp/config/p2r-audio_prebuilt.mk)

# Versioning
$(call inherit-product, vendor/p2r-aosp/config/p2r-versioning.mk)
