#
# Copyright (C) 2017 The Google Pixel2ROM Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from device AOSP product configuration
$(call inherit-product, device/lge/bullhead/aosp_bullhead.mk)

# Perform overrides
PRODUCT_NAME := bullhead
PRODUCT_DEVICE := bullhead
PRODUCT_BRAND := google
PRODUCT_MODEL := Nexus 5X
PRODUCT_MANUFACTURER := LGE
PRODUCT_RESTRICT_VENDOR_FILES := false
PRODUCT_USES_SOURCE_VENDOR_IMAGE ?= false

# Inherit from Pixel2ROM common configuration
$(call inherit-product, vendor/p2r-aosp/config/p2r-common.mk)
# Inherit TheMuppets proprietary vendor blobs
$(call inherit-product-if-exists, vendor/lge/bullhead/bullhead-vendor.mk)
# Inherit from vendor image build config
ifeq ($(PRODUCT_USES_SOURCE_VENDOR_IMAGE),true)
$(call inherit-product-if-exists, vendor/lge/bullhead/bullhead-vendorimage.mk)
endif

# Build properties overrides
PRODUCT_BUILD_PROP_OVERRIDES += \
    BUILD_FINGERPRINT=google/bullhead/bullhead:8.1.0/OPM6.171019.030.E1/4805388:user/release-keys \
    PRIVATE_BUILD_DESC="bullhead-user 8.1.0 OPM6.171019.030.E1 4805388 release-keys" \
    PRODUCT_NAME="walleye" \
    PRODUCT_MODEL="Pixel 2"

# Vendor build.prop fingerprint override if needed
ifeq ($(PRODUCT_USES_SOURCE_VENDOR_IMAGE),true)
VENDOR_BUILD_FINGERPRINT_OVERRIDE := "google/bullhead/bullhead:8.1.0/OPM6.171019.030.E1/4805388:user/release-keys"
endif
